

// var SQ_CALLBACK_URL = process.env.SQ_CALLBACK_URL;
// var SQ_CLIENT_ID = process.env.SQ_CLIENT_ID;
import { SQ_CALLBACK_URL, SQ_CLIENT_ID } from '../constants'

var transactionTotal = "100000";
var currencyCode = "UGX";
var sdkVersion = "v2.0";
var iosSDKVersion = "1.3";

export const authorize = () => {
  const perms = [
    'MERCHANT_PROFILE_WRITE', 'MERCHANT_PROFILE_READ', 'MERCHANT_PROFILE_WRITE',
    'ORDERS_WRITE', 'ORDERS_READ', 'PAYMENTS_WRITE',
    'INVENTORY_WRITE', 'INVENTORY_READ',
    'ITEMS_WRITE', 'ITEMS_READ',
    'PAYMENTS_READ', 'PAYMENTS_WRITE', 'PAYMENTS_WRITE_ADDITIONAL_RECIPIENTS',
    'CUSTOMERS_READ', 'CUSTOMERS_WRITE'
  ]

  const clientid = SQ_CLIENT_ID
  const url = `https://connect.squareupsandbox.com/oauth2/authorize?client_id=${clientid}&scope=${perms.join('%20')}&state=${String(Math.random())}`
  window.open(url);
}

export const openURL = () => {
  // Configure the allowable tender types
  var tenderTypes =
  `com.squareup.pos.TENDER_CARD, \
    com.squareup.pos.TENDER_CARD_ON_FILE, \
    com.squareup.pos.TENDER_CASH, \  
    com.squareup.pos.TENDER_OTHER`;

  var posUrl =
    "intent:#Intent;" +
    "action=com.squareup.pos.action.CHARGE;" +
    "package=com.squareup;" +
    "S.com.squareup.pos.WEB_CALLBACK_URI=" + SQ_CALLBACK_URL + ";" +
    "S.com.squareup.pos.CLIENT_ID=" + SQ_CLIENT_ID + ";" +
    "S.com.squareup.pos.API_VERSION=" + sdkVersion + ";" +
    "i.com.squareup.pos.TOTAL_AMOUNT=" + transactionTotal + ";" +
    "S.com.squareup.pos.CURRENCY_CODE=" + currencyCode + ";" +
    "S.com.squareup.pos.TENDER_TYPES=" + tenderTypes + ";" +
    "end";

  window.open(posUrl);
}

export const openURL_IOS = () => {
  var dataParameter = {
    amount_money: {
      amount: transactionTotal,
      currency_code: currencyCode
    },

    // Replace this value with your application's callback URL
    callback_url: SQ_CALLBACK_URL,

    // Replace this value with your application's ID
    client_id: SQ_CLIENT_ID,

    version: iosSDKVersion,
    notes: "notes for the transaction",
    options: {
      supported_tender_types: ["CREDIT_CARD","CASH","OTHER","SQUARE_GIFT_CARD","CARD_ON_FILE"]
    }
  };

  window.location =
    "square-commerce-v1://payment/create?data=" +
    encodeURIComponent(JSON.stringify(dataParameter));
}

export function getMobileOperatingSystem() {
  var userAgent = navigator.userAgent || navigator.vendor || window.opera;

      // Windows Phone must come first because its UA also contains "Android"
    if (/windows phone/i.test(userAgent)) {
        return "Windows Phone";
    }

    if (/android/i.test(userAgent)) {
        return "Android";
    }

    // iOS detection from: http://stackoverflow.com/a/9039885/177710
    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
        return "iOS";
    }

    return "unknown";
}
