import { getMobileOperatingSystem } from './auth'

const clientTransactionId = "com.squareup.pos.CLIENT_TRANSACTION_ID";
const transactionId = "com.squareup.pos.SERVER_TRANSACTION_ID";
const errorField = "com.squareup.pos.ERROR_CODE";

export function getUrlParams(URL) {
  var vars = {};
  var parts = URL.replace(/[?&]+([^=&]+)=([^&]*)/gi,
  function(m,key,value) {
    vars[key] = value;

  });
  return vars;
}
export function getTransactionInfo(URL) {
  var data = decodeURI(URL.searchParams.get("data"));

  console.log("data: " + data);
  var transactionInfo = JSON.parse(data);
  return transactionInfo;
}

export function handleSuccess(transactionInfo){
  var resultString ="";

  if (clientTransactionId in transactionInfo) {
    resultString += "Client Transaction ID: " + transactionInfo[clientTransactionId] + "<br>";
  }
  if (transactionId in transactionInfo) {
    resultString += "Transaction ID: " + transactionInfo[transactionId] + "      <br>";
  }
    else {
    resultString += "Transaction ID: NO CARD USED<br>";
  }
  return resultString;
}

// Makes an error string for error situation
export function handleError(transactionInfo){
  var resultString ="";

  if (errorField in transactionInfo) {
    resultString += "Client Transaction ID: " + transactionInfo[clientTransactionId] + "<br>";
  }
  if (transactionId in transactionInfo) {
    resultString += "Transaction ID: " + transactionInfo[transactionId] + "     <br>";
  }
    else {
    resultString += "Transaction ID: PROCESSED OFFLINE OR NO CARD USED<br>";
  }
  return resultString;
}

export function printResponse() {
  var responseUrl = window.location.href;
  var transactionInfo = getMobileOperatingSystem() ===  "iOS" ? getTransactionInfo(responseUrl) : getUrlParams(responseUrl);
  var resultString = "";

  if (errorField in transactionInfo) {
    resultString = handleError(transactionInfo);
  } else {
    resultString = handleSuccess(transactionInfo);
  }

  return resultString;
}
