const fs = require('fs');
const converter = require('json-2-csv');
// read the vendors file

const REGEXES = {
  date: /Date\s\d{2}\/\d{2}\/\d{2}.*Page\s.+/gi,
  ledger: /\s+V.+L.+\s*\n*(-{3,})\s{2}V(.*\n*\s*){4}(-{3,})/g,
  header: /(-{3,})\s{2}V(.*\n*\s*){4}(-{3,})/g,
  topTrim: /\s+V\sE\sN.+L.+\s*\n+(Vendor.*\s*\n*){2}/g,
  bottomTrim: /\n*\d{1,}\svendors\son\sfile\s*\n*.*\s*\n*/g,
  notes: /^\s*(Notes:|Note ID:)(\s*.*\r*\n*.*)+$/g,
  vendor: /Active\s+\d*(\.+\d)\d*/g,
  taxCode: /(?<=\s*)\w*\d*(?=\s+\d*(\.+\d)\d*)/,
  fax: /[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*\d+/,
  phone: /(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}/,
  street: /(\d*-*(\d{3,}|\w*))\s?(\w{0,5})\s([a-zA-Z]{2,30})\s([a-zA-Z]{2,15})\.?\s?(\w{0,5})/,
  zip: /\d{5}/
}

fs.readFile('./vendors.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  let result = parseVendors(data)
  // exportJson(JSON.stringify(result))
  exportCsv(result)
  // saveTxt(result)
})


const parseVendors = (data) => {
  let text = data

  // remove the date and page indicator row
  const dateRegex = REGEXES.date;
  text = text.replace(dateRegex, '')

  // remove the ledger header
  const endRegex = REGEXES.ledger;
  text = text.replace(endRegex, '')


  let headerRegex = REGEXES.header

  // remove the header if any is left
  if (text.search(headerRegex) !== -1) {
    text = text.replace(headerRegex, '')
  }

  // trim the top of the file
  let topTrimRegex = REGEXES.topTrim
  text = text.replace(topTrimRegex, '')
  
  // trim the bottom of the file
  let bottomTrimRegex = REGEXES.bottomTrim
  text = text.replace(bottomTrimRegex, '')

  // remove notes
  let notesRegex = /\s+(Note\sID|Notes):[\s\S]*?(?=\r\n\r\n.+(Active|Inactv))/g
  text = text.replace(notesRegex, '')

  text = text.trim()

  let textArray = text.split('\r\n\r\n')
  let textItems = textArray.map(vendor => vendor.split('\r\n'))

  let vendorArray = textItems.map(vendor => vendor.map(line => line.match(/(\S+\s{0,1})*\S+/g)).filter(l => l !== null));
  let dataArray = []
  // return vendorArray

  let categories = [ 'JUNE', 'INVO', 'NONIN', 'ACT', 'INV']
  let taxCodes = []

  vendorArray.forEach((vendor, i) => {
    let datum = {}
    let vendorId = vendor[0][0].startsWith('\u0012') ? vendor[0][0].substring(1): vendor[0][0]
    datum["Vendor-#"] = vendorId
    datum["Name"] = vendor[0][1]
    datum["Status"] = vendor[0].includes("Active") ? "Active": "Inactv"
    let statusIndex = vendor[0].findIndex(l => l == "Active" || l == "Inactv")
    datum["Unposted-bal"] = statusIndex !== -1 ? vendor[0][statusIndex + 1] : " "
    let categoryIndex = statusIndex -1
    if (statusIndex !== -1 && categories.includes(vendor[0][categoryIndex])){
      datum["Category"] = vendor[0][categoryIndex]
    } else {
      datum["Category"] = " "
    }
    let stateRegex = /^[a-zA-Z]{2}$/
    let zipRegex = /[0-9]{5}/g
    if (vendor[3] && vendor[3][1]) {
      let hasState = stateRegex.test(vendor[3][1])
      if (hasState) {
        datum["State"] = vendor[3][1]
        datum["City"] = vendor[3][0]
        datum["Zip-code"] = vendor[3][2].match(zipRegex) ? vendor[3][2].match(zipRegex)[0] : " "
      } else {
        datum["State"] = " "
        datum["City"] = " "
        datum["Zip-code"] = " "
      }
      
    } else {
      datum["State"] = " "
      datum["City"] = " "
      datum["Zip-code"] = " "
    }

    if (vendor[1] && vendor[1].find(item => item.match(REGEXES.phone))) {
      datum["Phone"] = vendor[1].find(item => item.match(REGEXES.phone)) ? vendor[1].find(item => item.match(REGEXES.phone)).match(REGEXES.phone)[0] : " " 
    } else {
      datum["Phone"] = " "
    }

    if (vendor[2] && vendor[2].find(i => i.match(/fax/i))) {
      datum["Fax"] = vendor[3] ? vendor[3].find(item => item.match(REGEXES.phone)) ? vendor[3].find(item => item.match(REGEXES.phone)).match(REGEXES.phone)[0] : " " : " "
    } else {
      datum["Fax"] = " "
    }

    const numRegex = /^\d*(,\d+)*\.\d+-*$/g
    let taxRegex = /^[A-Z]{0,4}\d{0,4}$/i
    if (vendor[1]) {
      let last = vendor[1][vendor[1].length - 1]
      let tax = last.match(numRegex) ? vendor[1][vendor[1].length - 2] : vendor[1][vendor[1].length - 3]
      if (tax && tax.match(taxRegex)) datum["Tax-ID-#"] = tax; else datum["Tax-ID-#"] = " "
    } else {
      datum["Tax-ID-#"] = " "
    }

    dataArray.push(datum)
  });
  return dataArray
}

const exportJson = (data) => {
  fs.writeFile('./vendors/result.json', data, err => {
    if (err) {
      console.error(err)
      return
    }
    //file written successfully
    console.log('Json written successfully')
  })
}

const exportCsv = (data) => {
  converter.json2csv(data, (err, csv) => {
    if (err) {
        throw err;
    }
    // print CSV string
    fs.writeFileSync('./vendors/results.csv', csv);
    // console.log(csv);
  });

}

const saveTxt = (text) => {
  fs.writeFile('./vendors/result.txt', text, err => {
    if (err) {
      console.error(err)
      return
    }
    //file written successfully
    console.log('file written successfully')
  })
}